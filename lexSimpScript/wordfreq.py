import sys
import nltk
from nltk.corpus import reuters
from nltk.tokenize import RegexpTokenizer
from nltk_contrib.readability.textanalyzer import syllables_en
import numpy as np
from sklearn.externals import joblib

CORPUS = reuters.words()

CORPUS = [w.lower() for w in CORPUS]
FD = nltk.FreqDist(CORPUS)
LENGTH = len(CORPUS)

def main():
    with open('test.txt', 'r') as raw:
        data=raw.read().replace('\n', ' ').decode('utf8')

    tokenizer = RegexpTokenizer(r'\w+')
    tokens = tokenizer.tokenize(data)
    # lower case all letter
    tokens = [w.lower() for w in tokens]

    print classify_word_difficulty(tokens)


        # features = np.append(features, [find_features(x)], axis=0)

    # target = lm.predict(features[:,1:].astype('float'))

    # print target

def classify_word_difficulty(words_list):

    lm = joblib.load('models/word_grade')

    difficulty =  np.empty([0,2])

    for x in words_list:
        value = [x, max(0, lm.predict(find_features(x))[0][0])]
        difficulty = np.append(difficulty, [value], axis=0)

    return difficulty

def find_features(word):
    return np.array([find_freq(word), len(word), find_num_syllable(word)]).reshape(1, -1)

def find_freq(word):
    freq_count = float(FD[word])
    freq = freq_count / LENGTH

    return freq

def find_num_syllable(data):
    return syllables_en.count(data)
    # print split_word.split('-')


if __name__ == '__main__':
    sys.exit(main())

