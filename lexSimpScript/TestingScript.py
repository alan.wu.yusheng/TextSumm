import sys
import os
# from lexenstein.morphadorner import MorphAdornerToolkit
# from generator.YamamotoGenerator import YamamotoGenerator
# from generator.MerriamGenerator import MerriamGenerator
# from generator.WordnetGenerator import WordnetGenerator
# from selector.AluisioSelector import AluisioSelector
# from selector.WordVectorSelector import WordVectorSelector
# from selector.WSDSelector import WSDSelector
# from lexenstein.rankers import *
# from lexenstein.features import *
from evaluators.eval import PipelineEvaluator
# from generator.WNGenerator import WNGenerator
from ranker.PairWiseRanker import PairWiseRanker
# import pickle
from nltk.corpus import gutenberg

tools_dir = '/home/candiii/dev/tools/'

results_dir = 'results/test/'

def main():
    standards = []
    for fn in os.listdir('./standard/'):
        if fn != 'DB.txt':
            standards.append("standard/" + fn)

    # standards.append("standard/DB.txt")


    # default_generator_test(standards, results_dir)

    # default_selector_test(results_dir)
    #
    default_ranker_runner(results_dir)
    #
    eval_res = pipeline_evaluation(results_dir + "ranker/")

    resultsToCSV(eval_res, results_dir + "s_final.txt")

    # run_self_algorithm(standards, results_dir)

    custom_ranker_runner(results_dir)
    #
    # eval_res = pipeline_evaluation(results_dir + "custom_rank/", 1)
    #
    # resultsToCSV(eval_res, results_dir + "s_self_final.txt",1)

    # ranker = PairWiseRanker(gutenberg.words())

    # ranker.train_model('standard/semeval_train.txt', 'models/pwranker.bin')

    # ranking = ranker.getRankings("results/custom/DeBelder.txt")
    #
    # toVictorFormat('standard/DeBelder.txt', ranking, results_dir + "test.txt")

def run_self_algorithm(standards, output_dir):
    m = MorphAdornerToolkit(tools_dir + 'MorphAdornerToolkit/')

    generator = WNGenerator(m);

    results_dir = output_dir + "custom/"
    for corpus in standards:
        standard_name = corpus.split('/')[1]
        print standard_name

        if os.path.isfile(results_dir + standard_name):
            continue

        subs = generator.getSubstitutions(corpus)

        if not os.path.exists(results_dir):
            os.makedirs(results_dir)

        generator.toVictorFormat(subs,results_dir + standard_name)

def pipeline_evaluation(results_dir, level=3):
    pe = PipelineEvaluator()

    results = []

    for fn in os.listdir(results_dir):
        split_name = fn.split("_")[level:]
        standard_name = "standard/" + '_'.join(split_name)


        # standard_name = "standard/" +  fn;
        ranking = toRankingFormat(results_dir + fn)

        precision, accuracy, changed, \
        trank1, trank2, trank3, \
        recall1, recall2, recall3 = pe.evaluateAll(standard_name, ranking)

        eval = [fn.split('.')[0], precision, accuracy, changed, trank1, trank2, trank3, recall1, recall2, recall3]

        results.append(eval);

    return results

def custom_ranker_runner(output_dir):
    # m = MorphAdornerToolkit(tools_dir + 'MorphAdornerToolkit/')

    input_dir = output_dir + "custom/"
    ranked_dir = output_dir + "custom_rank/"

    rankers = []

    # rankers.append(('bott', BottRanker('models/lms.bin')))
    rankers.append(('pairwise', PairWiseRanker(gutenberg.words(), 'models/pwranker.bin')))

    if not os.path.exists(ranked_dir):
        os.mkdir(ranked_dir)

    for fn in os.listdir(input_dir):
        # standard_name = '_'.join(fn.split("_")[2:])
        standard_name = fn;

        for name, ranker in rankers:
            print name, standard_name
        #
            if os.path.isfile(ranked_dir + name + "_" + fn):
                continue
            rankings = ranker.getRankings(input_dir + fn)
            toVictorFormat('standard/'+standard_name, rankings, ranked_dir + name + "_" + fn)


def default_ranker_runner(output_dir):
    # m = MorphAdornerToolkit(tools_dir + 'MorphAdornerToolkit/')

    selector_dir = output_dir + "selector/"
    ranker_dir = output_dir + "ranker/"

    rankers = []

    # rankers.append(('bott', BottRanker('models/lms.bin')))
    rankers.append(('pairwise', PairWiseRanker(gutenberg.words(), 'models/pwranker.bin')))

    if not os.path.exists(ranker_dir):
        os.mkdir(ranker_dir)

    for fn in os.listdir(selector_dir):
        standard_name = '_'.join(fn.split("_")[2:])
        # standard_name = fn;


        for name, ranker in rankers:
            print name, standard_name

            if os.path.isfile(ranker_dir + name + "_" + fn):
                continue
            rankings = ranker.getRankings(selector_dir + fn)
            toVictorFormat('standard/'+standard_name, rankings, ranker_dir + name + "_" + fn)


def default_selector_test(output_dir):
    generator_dir = output_dir + "generator/"
    selector_dir = output_dir + "selector/"

    selectors = []

    selectors.append(('wsd', WSDSelector('lesk')));
    selectors.append(('aluisio', AluisioSelector('models/cond_prob.bin')))
    selectors.append(('wordvector', WordVectorSelector('models/word_vector_model.bin')))


    if not os.path.exists(selector_dir):
        os.makedirs(selector_dir)

    for fn in os.listdir(generator_dir):
        print fn
        standard_name = '_'.join(fn.split("_")[1:])
        subs = pickle.load(open(generator_dir + fn, "rb" ))

        for name, select in selectors:
            print name, standard_name

            if not os.path.isfile(selector_dir + name + "_"+ fn):
                selected = select.selectCandidates(subs,"standard/" + standard_name)
                select.toVictorFormat("standard/" + standard_name, selected, selector_dir + name + "_"+ fn)


def default_generator_test(standards, output_dir):
    m = MorphAdornerToolkit(tools_dir + 'MorphAdornerToolkit/')

    generators = []

    generators.append(("yamamoto", YamamotoGenerator(m, 'c14cf9be-c06c-4619-8f3b-b5e4122614e7')))
    generators.append(("merriam", MerriamGenerator(m, '5a7b123d-79c5-4f0e-ba7c-3fb899dae261')))
    generators.append(("wordnet", WordnetGenerator(m)))
    # generators.append(("mine", WNGenerator(m)))

    generator_dir = output_dir + "generator/"
    for name, generator in generators:
        for corpus in standards:
            standard_name = corpus.split('/')[1]
            print name, standard_name

            if os.path.isfile(generator_dir + name + "_"+ standard_name):
                continue

            subs = generator.getSubstitutions(corpus)

            if not os.path.exists(generator_dir):
                os.makedirs(generator_dir)

            with open(generator_dir + name + "_" + standard_name, 'w') as file:
                pickle.dump(subs, file)

    conglomerate = {}

    for fn in os.listdir(generator_dir):
        standard_name = "_".join(fn.split("_")[1:])
        if standard_name != conglomerate.keys():
            conglomerate[standard_name] = []
        conglomerate[standard_name].append(fn);

    for standard in conglomerate:
        substitutions = []
        for fn in conglomerate[standard]:
            substitutions.append(pickle.load(open(generator_dir + fn, "rb" )))

        conglomerate_subs(substitutions, generator_dir, standard)



def conglomerate_subs(subs, output_dir, file_name):
    merged = set([])
    for cands in subs:
        merged = merged.union(cands.keys())

    final = {}

    for x in merged:
        if x not in final.keys():
            final[x] = set([])

            for cads in subs:
                if x in cads.keys():
                    final[x] = final[x].union(cads[x])
    with open(output_dir + "all_" + file_name, 'w') as file:
        pickle.dump(final, file)

def resultsToCSV(results_array, output_file, level=3):
    o = open(output_file, 'w')

    o.write('standard ,name, precision, accuracy, changed, trank1, trank2, trank3, recall1, recall2, recall3\n')
    for r in results_array:
        name = '_'.join(r[0].split('_')[level:])
        method = r[0].split('_'+name)[0]
        line = name + ','+ method + ',' + ','.join([str(x) for x in r[1:]]);
        o.write(line + '\n')
    o.close()



def toRankingFormat(victor_corpus):
    f = open(victor_corpus)
    result = []
    for line in f:
        # Get all substitutions in ranking instance:
        data = line.strip().split('\t')
        substitutions = data[3:len(data)]

        rankings = []

        for j in range(0, len(substitutions)):
            substitution = substitutions[j].split(':')[1].strip()
            rankings.append(substitution)

        # Add them to result:
        result.append(rankings)
    f.close()

    return result;

def toVictorFormat(victor_corpus, substitutions, output_path):
    """
    Saves a set of selected substitutions in a file in VICTOR format.

    @param victor_corpus: Path to the corpus in the VICTOR format to which the substitutions were selected.
    @param substitutions: The vector of substitutions selected for the VICTOR corpus.
    @param output_path: The path in which to save the resulting VICTOR corpus.
    @param addTargetAsCandidate: If True, adds the target complex word of each instance as a candidate substitution.
    """
    o = open(output_path, 'w')
    f = open(victor_corpus)
    for subs in substitutions:
        data = f.readline().strip().split('\t')
        sentence = data[0].strip()
        target = data[1].strip()
        head = data[2].strip()

        newline = sentence + '\t' + target + '\t' + head + '\t'
        for i,sub in enumerate(subs):
            newline += str(i+1) + ':'+sub + '\t'
        o.write(newline.strip() + '\n')
    f.close()
    o.close()


if __name__ == '__main__':
    sys.exit(main())
