from nltk.corpus import brown
from pos_tag.lapos import Lapos
import nltk
from pywsd.lesk import simple_lesk
import autocorrect
import tqdm


class WNGenerator:
    def __init__(self, mat):

        # , pos_model, stanford_tagger, java_path
        """
        Creates a WordnetGenerator instance.

        @param mat: MorphAdornerToolkit object.
        @param nc: NorvigCorrector object.
        @param pos_model: Path to a POS tagging model for the Stanford POS Tagger.
        The models can be downloaded from the following link: http://nlp.stanford.edu/software/tagger.shtml
        @param stanford_tagger: Path to the "stanford-postagger.jar" file.
        The tagger can be downloaded from the following link: http://nlp.stanford.edu/software/tagger.shtml
        @param java_path: Path to the system's "java" executable.
        Can be commonly found in "/usr/bin/java" in Unix/Linux systems, or in "C:/Program Files/Java/jdk_version/java.exe" in Windows systems.
        """
        self.mat = mat
        self.tagger = Lapos('../../lapos/', 'model_wsj02-21/')
        # self.model = gensim.models.Word2Vec.load('models/brown_model.bin')
        # self.cwi = ComplexWordIdentifer(brown.words())

    def getSubstitutions(self, victor_corpus):
        """
        Generates substitutions for the target words of a corpus in VICTOR format.

        @param victor_corpus: Path to a corpus in the VICTOR format.
        For more information about the file's format, refer to the LEXenstein Manual.
        @return: A dictionary that assigns target complex words to sets of candidate substitutions.
        Example: substitutions['perched'] = {'sat', 'roosted'}
        """

        # Get initial set of substitutions:
        print('Getting initial set of substitutions...')
        substitutions_initial = self.getInitialSet(victor_corpus)
        # #Get final substitutions:
        print('Inflecting substitutions...')
        substitutions_inflected = self.getInflectedSet(substitutions_initial)
        #

        # #Return final set:
        # print('Finished!')
        return substitutions_inflected

    def getInflectedSet(self, subs):
        # Create list of targets:
        targets = []

        # Create lists for inflection:

        final_substitutions = []

        # Fill lists:
        for target_sub in tqdm.tqdm(subs, total=len(subs), leave=True):
            target = target_sub["target"]

            pos = target_sub["target_pos"]

            # Get cands for a target and tag combination:
            cands = target_sub["candidates"]

            final_cands = set([]);

            toPA = []
            toPRPA = []
            toPAPA = []
            toPE = []
            toPR = []

            split = []
            first = []
            for words in cands:
                w = words.split(' ')
                split.append(w)
                first.append(w[0])
            # Add candidates to lists:
            if pos == 'NNS':
                plurals = self.mat.inflectNouns(cands, 'plural')
                final_cands = set(plurals)
            elif pos == 'VBD':
                pas = self.mat.conjugateVerbs(first, 'PAST', 'FIRST_PERSON_SINGULAR')
                for i,split_words in enumerate(split):
                    final_cands.add(pas[i] + " " + " ".join(split_words[1:]))
            elif pos == 'VBG':
                prpas = self.mat.conjugateVerbs(first, 'PRESENT_PARTICIPLE', 'FIRST_PERSON_SINGULAR')
                for i,split_words in enumerate(split):
                    final_cands.add(prpas[i] + " " + " ".join(split_words[1:]))
            elif pos == 'VBN':
                papas = self.mat.conjugateVerbs(first, 'PAST_PARTICIPLE', 'FIRST_PERSON_SINGULAR')
                for i,split_words in enumerate(split):
                    final_cands.add(papas[i] + " " + " ".join(split_words[1:]))
            elif pos == 'VBP':
                pes = self.mat.conjugateVerbs(first, 'PRESENT', 'FIRST_PERSON_SINGULAR')
                for i,split_words in enumerate(split):
                    final_cands.add(pes[i] + " " + " ".join(split_words[1:]))
            elif pos == 'VBZ':
                prs = self.mat.conjugateVerbs(first, 'PRESENT', 'THIRD_PERSON_SINGULAR')
                for i,split_words in enumerate(split):
                    final_cands.add(prs[i] + " " + " ".join(split_words[1:]))
            else:
               final_cands = set(cands);

            final_cands.add(target);

            final_cands = [x.strip() for x in final_cands]
            final_cands = list(set(final_cands))

            # cand_score = []
            #
            # for cand in final_cands:
            #     similarity = 0.8
            #     try:
            #         similarity = self.model.similarity(cand, target)
            #     except:
            #         pass
            #     aoa = self.cwi.predict_aoa(cand)
            #     if aoa == 0:
            #         aoa = 0.1
            #     aoa_score = 1 / aoa
            #     diff = 1.0
            #     if cand == target:
            #         diff = 0.7
            #     score = similarity * aoa_score * diff
            #     cand_score.append((cand, score))

            target_sub["candidates"] = final_cands

            # Add final cands to final substitutions:
            final_substitutions.append(target_sub)
        return final_substitutions


    def getInitialSet(self, victor_corpus):
        substitutions_initial = []
        lexf = open(victor_corpus)
        sents = []
        targets = []
        heads = []
        for line in lexf:
            data = line.strip().split('\t')
            sent = data[0].strip()
            target = data[1].strip()
            head = int(data[2].strip())
            sents.append(sent)
            targets.append(target)
            heads.append(head)
        lexf.close()

        tagged_sents = [];

        for sent in sents:
            result = self.tagger.pos_sentence(sent)
            tagged_sents.append([nltk.tag.str2tuple(t) for t in result.split()])
        # tagged_sents = self.tagger.tag_sents(sents)

        for i in tqdm.tqdm(range(0, len(sents)), total=len(sents), leave=True):
            try:
                target = targets[i]
                head = heads[i]

                for word, pos in tagged_sents[i]:
                    if word == target:
                        target_pos = pos;
                # target_pos = str(tagged_sents[i][head][1])

                target_syn = simple_lesk(sents[i], target)
                definition = target_syn.definition()

                hypernyms =  target_syn.hypernyms()

                #
                # for syn in syns:
                # 	print syn.definition()
                # print syns

                cands = []
                for lem in target_syn.lemmas():
                    candidate = self.cleanLemma(lem.name())
                    cands.append(candidate)

                for synset in hypernyms:
                    for lem in synset.lemmas():
                        candidate = self.cleanLemma(lem.name())
                        cands.append(candidate)
                # if len(cands)>0:
                # 	if target in substitutions_initial:
                # 		substitutions_initial[target][target_pos] = cands
                # 	else:
                # 		substitutions_initial[target] = {target_pos:cands}
                res = {
                    'sent': sents[i],
                    'tagged_sents': tagged_sents[i],
                    'target': target,
                    'head': head,
                    'target_pos': target_pos,
                    'target_sense': target_syn.name(),
                    'candidates': cands,
                    'definition': definition
                }

                # print res

                substitutions_initial.append(res)
            except:
                target_pos = str(tagged_sents[i][head][1])
                res = {
                    'sent': sents[i],
                    'tagged_sents': tagged_sents[i],
                    'target_pos': target_pos,
                    'target': targets[i],
                    'head': heads[i],
                    'candidates': [targets[i]]
                }
                substitutions_initial.append(res)
                continue
        return substitutions_initial

    def addToExtended(self, target, tag, cands, subs):
        if target not in subs:
            subs[target] = {tag: cands}
        else:
            if tag not in subs[target]:
                subs[target][tag] = cands
            else:
                subs[target][tag].extend(cands)


    def correctWords(self, words):
        result = []
        for word in words:
            result.append(autocorrect.spell(word))
        return result


    def cleanLemma(self, lem):
        result = ''
        aux = lem.strip().split('_')
        for word in aux:
            result += word + ' '
        return result.strip()


    def getWordnetPOS(self, pos):
        if pos[0] == 'N' or pos[0] == 'V' or pos == 'RBR' or pos == 'RBS':
            return pos[0].lower()
        elif pos[0] == 'J':
            return 'a'
        else:
            return None

    def toVictorFormat(self, subs, output_path):
        o = open(output_path, 'w')
        for entry in subs:
            sentence = entry["sent"]
            target = entry["target"]
            head = str(entry["head"])
            candidates = list(entry["candidates"])

            newline = sentence + '\t' + target + '\t' + head + '\t'
            for i, sub in enumerate(candidates):
                newline += '0:' + sub + '\t'
            o.write(newline.strip() + '\n')
        o.close()
