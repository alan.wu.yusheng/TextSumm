import xml.etree.ElementTree as ET
import re
import urllib2 as urllib
import codecs
import autocorrect
import tqdm
import pickle

class MerriamGenerator:

	def __init__(self, mat, thesaurus_key):
		"""
		Creates a MerriamGenerator instance.

		@param mat: MorphAdornerToolkit object.
		@param thesaurus_key: Key for the Merriam Thesaurus.
		For more information on how to get the key for free, please refer to the LEXenstein Manual
		@param nc: NorvigCorrector object.
		"""
		self.mat = mat
		self.thesaurus_key = thesaurus_key

	def getSubstitutions(self, victor_corpus):
		"""
		Generates substitutions for the target words of a corpus in VICTOR format.

		@param victor_corpus: Path to a corpus in the VICTOR format.
		For more information about the file's format, refer to the LEXenstein Manual.
		@return: A dictionary that assigns target complex words to sets of candidate substitutions.
		Example: substitutions['perched'] = {'sat', 'roosted'}
		"""
		#Get initial set of substitutions:
		print('Getting initial set of substitutions...')
		substitutions_initial = self.getInitialSet(victor_corpus)

		standard_name = victor_corpus.split('/')[1]

		with open("results/temp/merriam_" + standard_name, 'w') as file:
			pickle.dump(substitutions_initial, file)

		#Get final substitutions:
		print('Inflecting substitutions...')
		substitutions_inflected = self.getInflectedSet(substitutions_initial)

		#Return final set:
		print('Finished!')
		return substitutions_inflected

	def getInflectedSet(self, result):
		final_substitutions = {}

		#Get inflections:
		allkeys = sorted(list(result.keys()))

		singulars = {}
		plurals = {}
		verbs = {}

		singularsk = {}
		pluralsk = {}
		verbsk = {}

		for i in range(0, len(allkeys)):
			key = allkeys[i]
			leftw = key

			for leftp in result[leftw]:
				if leftp.startswith('n'):
					if leftp=='nns':
						pluralsk[leftw] = set([])
						for subst in result[key][leftp]:
							plurals[subst] = set([])
					else:
						singularsk[leftw] = set([])
						for subst in result[key][leftp]:
							singulars[subst] = set([])
				elif leftp.startswith('v'):
					verbsk[leftw] = {}
					for subst in result[key][leftp]:
						verbs[subst] = {}

		#------------------------------------------------------------------------------------------------

		#Generate keys input:
		singkeys = sorted(list(singularsk.keys()))
		plurkeys = sorted(list(pluralsk.keys()))
		verbkeys = sorted(list(verbsk.keys()))

		#Get stems:
		singstems, plurstems, verbstems = self.getStems(singkeys, plurkeys, verbkeys)

		#Get plurals:
		singres = self.getPlurals(singstems)

		#Get singulars:
		plurres = self.getSingulars(plurstems)

		#Get verb inflections:
		verbres1, verbres2, verbres3, verbres4, verbres5 = self.getInflections(verbstems)

		#Add information to dictionaries:
		for i in range(0, len(singkeys)):
			k = singkeys[i]
			singre = singres[i]
			singularsk[k] = singre
		for i in range(0, len(plurkeys)):
			k = plurkeys[i]
			plurre = plurres[i]
			pluralsk[k] = plurre
		for i in range(0, len(verbkeys)):
			k = verbkeys[i]
			verbre1 = verbres1[i]
			verbre2 = verbres2[i]
			verbre3 = verbres3[i]
			verbre4 = verbres4[i]
			verbre5 = verbres5[i]
			verbsk[k] = {'PAST_PERFECT_PARTICIPLE': verbre1, 'PAST_PARTICIPLE': verbre2, 'PRESENT_PARTICIPLE': verbre3, 'PRESENT': verbre4, 'PAST': verbre5}

		#------------------------------------------------------------------------------------------------

		#Generate substs input:
		singkeys = sorted(list(singulars.keys()))
		plurkeys = sorted(list(plurals.keys()))
		verbkeys = sorted(list(verbs.keys()))

		#Get stems:
		singstems, plurstems, verbstems = self.getStems(singkeys, plurkeys, verbkeys)

		#Get plurals:
		singres = self.getPlurals(singstems)

		#Get singulars:
		plurres = self.getSingulars(plurstems)

		#Get verb inflections:
		verbres1, verbres2, verbres3, verbres4, verbres5 = self.getInflections(verbstems)

		#Add information to dictionaries:
		for i in range(0, len(singkeys)):
			k = singkeys[i]
			singre = singres[i]
			singulars[k] = singre
		for i in range(0, len(plurkeys)):
			k = plurkeys[i]
			plurre = plurres[i]
			plurals[k] = plurre
		for i in range(0, len(verbkeys)):
			k = verbkeys[i]
			verbre1 = verbres1[i]
			verbre2 = verbres2[i]
			verbre3 = verbres3[i]
			verbre4 = verbres4[i]
			verbre5 = verbres5[i]
			verbs[k] = {'PAST_PERFECT_PARTICIPLE': verbre1, 'PAST_PARTICIPLE': verbre2, 'PRESENT_PARTICIPLE': verbre3, 'PRESENT': verbre4, 'PAST': verbre5}

		#------------------------------------------------------------------------------------------------

		#Generate final substitution list:
		for i in tqdm.tqdm(range(0, len(allkeys)), total=len(allkeys), leave=True):
			key = allkeys[i]
			leftw = key
			for leftp in result[leftw]:

				#Add final version to candidates:
				if leftw not in final_substitutions:
					final_substitutions[leftw] = result[key][leftp]
				else:
					final_substitutions[leftw] = final_substitutions[leftw].union(result[key][leftp])
				#If left is a noun:
				if leftp.startswith('n'):
					#If it is a plural:
					if leftp=='nns':
						plurl = pluralsk[leftw]
						newcands = set([])
						for candidate in result[key][leftp]:
							candplurl = plurals[candidate]
							newcands.add(candplurl)
						if plurl not in final_substitutions:
							final_substitutions[plurl] = newcands
						else:
							final_substitutions[plurl] = final_substitutions[plurl].union(newcands)
					#If it is singular:
					else:
						singl = singularsk[leftw]
						newcands = set([])
						for candidate in result[key][leftp]:
							candsingl = singulars[candidate]
							newcands.add(candsingl)
						if singl not in final_substitutions:
							final_substitutions[singl] = newcands
						else:
							final_substitutions[singl] = final_substitutions[singl].union(newcands)
				#If left is a verb:
				elif leftp.startswith('v'):
					for verb_tense in ['PAST_PERFECT_PARTICIPLE', 'PAST_PARTICIPLE', 'PRESENT_PARTICIPLE', 'PRESENT', 'PAST']:
						tensedl = verbsk[leftw][verb_tense]
						newcands = set([])
						for candidate in result[key][leftp]:
							candtensedl = verbs[candidate][verb_tense]
							newcands.add(candtensedl)
						if tensedl not in final_substitutions:
							final_substitutions[tensedl] = newcands
						else:
							final_substitutions[tensedl] = final_substitutions[tensedl].union(newcands)
		return final_substitutions

	def getInflections(self, verbstems):
		data1 = self.mat.conjugateVerbs(verbstems, 'PAST_PERFECT_PARTICIPLE', 'FIRST_PERSON_SINGULAR')
		data2 = self.mat.conjugateVerbs(verbstems, 'PAST_PARTICIPLE', 'FIRST_PERSON_SINGULAR')
		data3 = self.mat.conjugateVerbs(verbstems, 'PRESENT_PARTICIPLE', 'FIRST_PERSON_SINGULAR')
		data4 = self.mat.conjugateVerbs(verbstems, 'PRESENT', 'FIRST_PERSON_SINGULAR')
		data5 = self.mat.conjugateVerbs(verbstems, 'PAST', 'FIRST_PERSON_SINGULAR')
		return self.correctWords(data1), self.correctWords(data2), self.correctWords(data3), self.correctWords(data4), self.correctWords(data5)

	def getSingulars(self, plurstems):
		data = self.mat.inflectNouns(plurstems, 'singular')
		return self.correctWords(data)

	def getPlurals(self, singstems):
		data = self.mat.inflectNouns(singstems, 'plural')
		return self.correctWords(data)

	def getStems(self, sings, plurs, verbs):
		data = self.mat.lemmatizeWords(sings+plurs+verbs)
		rsings = []
		rplurs = []
		rverbs = []
		c = -1
		for sing in sings:
			c += 1
			if len(data[c])>0:
				rsings.append(data[c])
			else:
				rsings.append(sing)
		for plur in plurs:
			c += 1
			if len(data[c])>0:
				rplurs.append(data[c])
			else:
				rplurs.append(plur)
		for verb in verbs:
			c += 1
			if len(data[c])>0:
				rverbs.append(data[c])
			else:
				rverbs.append(verb)
		return self.correctWords(rsings), self.correctWords(rplurs), self.correctWords(rverbs)

	def getInitialSet(self, victor_corpus):
		substitutions_initial = {}


		num_lines = sum(1 for line in open(victor_corpus))

		lex = open(victor_corpus)
		for line in tqdm.tqdm(lex, total=num_lines, leave=True):
			data = line.strip().split('\t')
			target = data[1].strip()
			url = 'http://www.dictionaryapi.com/api/v1/references/thesaurus/xml/' + target + '?key=' + self.thesaurus_key
			conn = urllib.urlopen(url)
			root = ET.fromstring(conn.read())
			root = root.findall('entry')

			cands = {}
			if len(root)>0:
				for root_node in root:
					node_pos = root_node.find('fl')
					if node_pos != None:
						node_pos = node_pos.text.strip()[0].lower()
						if node_pos not in cands:
							cands[node_pos] = set([])
					for sense in root_node.iter('sens'):
						syn = sense.findall('syn')[0]
					res = ''
					for snip in syn.itertext():
						res += snip + ' '
					finds = re.findall('\([^\)]+\)', res)
					for find in finds:
						res = res.replace(find, '')

					synonyms = [s.strip() for s in res.split(',')]

					for synonym in synonyms:
						if len(synonym.split(' '))==1:
							try:
								test = codecs.ascii_encode(synonym)
								cands[node_pos].add(synonym)
							except UnicodeEncodeError:
								cands = cands
			for pos in cands:
				if target in cands[pos]:
					cands[pos].remove(target)
			if len(cands.keys())>0:
				substitutions_initial[target] = cands
		lex.close()
		return substitutions_initial

	def correctWords(self, words):
		result = []
		for word in words:
			result.append(autocorrect.spell(word))
		return result