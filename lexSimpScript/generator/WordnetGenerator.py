from nltk.corpus import wordnet as wn

import autocorrect
from pos_tag.lapos import Lapos
import nltk
import tqdm
import pickle
import os

class WordnetGenerator:
    def __init__(self, mat):
        """
        Creates a WordnetGenerator instance.

        @param mat: MorphAdornerToolkit object.
        @param nc: NorvigCorrector object.
        @param pos_model: Path to a POS tagging model for the Stanford POS Tagger.
        The models can be downloaded from the following link: http://nlp.stanford.edu/software/tagger.shtml
        @param stanford_tagger: Path to the "stanford-postagger.jar" file.
        The tagger can be downloaded from the following link: http://nlp.stanford.edu/software/tagger.shtml
        @param java_path: Path to the system's "java" executable.
        Can be commonly found in "/usr/bin/java" in Unix/Linux systems, or in "C:/Program Files/Java/jdk_version/java.exe" in Windows systems.
        """
        self.mat = mat
        self.tagger = Lapos('../../lapos/', 'model_wsj02-21/')

    def getSubstitutions(self, victor_corpus):
        """
        Generates substitutions for the target words of a corpus in VICTOR format.

        @param victor_corpus: Path to a corpus in the VICTOR format.
        For more information about the file's format, refer to the LEXenstein Manual.
        @return: A dictionary that assigns target complex words to sets of candidate substitutions.
        Example: substitutions['perched'] = {'sat', 'roosted'}
        """

        # Get initial set of substitutions:
        print('Getting initial set of substitutions...')
        standard_name = victor_corpus.split('/')[1]

        if os.path.isfile("results/temp/wordnet_" + standard_name):
            substitutions_initial = pickle.load(open("results/temp/wordnet_" + standard_name, 'rb'))
        else:
            substitutions_initial = self.getInitialSet(victor_corpus)


        with open("results/temp/wordnet_" + standard_name, 'w') as file:
            pickle.dump(substitutions_initial, file)

        # Get final substitutions:
        print('Inflecting substitutions...')
        substitutions_inflected = self.getInflectedSet(substitutions_initial)

        # Return final set:
        print('Finished!')
        return substitutions_inflected

    def getInflectedSet(self, subs):
        # Create list of targets:
        targets = []

        # Create lists for inflection:
        toNothing = []
        toSingular = []
        toPlural = []
        toPAPEPA = []
        toPA = []
        toPRPA = []
        toPAPA = []
        toPE = []
        toPR = []
        toComparative = []
        toSuperlative = []
        toOriginal = []

        # Fill lists:
        for target in subs:
            targets.append(target)
            for pos in subs[target]:
                # Get cands for a target and tag combination:
                cands = list(subs[target][pos])

                # Add candidates to lists:
                if pos == 'NN':
                    toSingular.extend(cands)
                elif pos == 'NNS':
                    toPlural.extend(cands)
                elif pos == 'VB':
                    toPAPEPA.extend(cands)
                elif pos == 'VBD':
                    toPA.extend(cands)
                    toPAPA.extend(cands)
                elif pos == 'VBG':
                    toPRPA.extend(cands)
                elif pos == 'VBN':
                    toPA.extend(cands)
                    toPAPA.extend(cands)
                elif pos == 'VBP':
                    toPE.extend(cands)
                elif pos == 'VBZ':
                    toPR.extend(cands)
                elif pos == 'JJR' or pos == 'RBR':
                    toComparative.extend(cands)
                elif pos == 'JJS' or pos == 'RBS':
                    toSuperlative.extend(cands)
                else:
                    toNothing.extend(cands)

                    # Lemmatize targets:
        targetsL = self.mat.lemmatizeWords(targets)

        # Lemmatize words:
        toNothingL = self.correctWords(self.mat.lemmatizeWords(toNothing))
        toSingularL = self.correctWords(self.mat.lemmatizeWords(toSingular))
        toPluralL = self.correctWords(self.mat.lemmatizeWords(toPlural))
        toPAPEPAL = self.correctWords(self.mat.lemmatizeWords(toPAPEPA))
        toPAL = self.correctWords(self.mat.lemmatizeWords(toPA))
        toPRPAL = self.correctWords(self.mat.lemmatizeWords(toPRPA))
        toPAPAL = self.correctWords(self.mat.lemmatizeWords(toPAPA))
        toPEL = self.correctWords(self.mat.lemmatizeWords(toPE))
        toPRL = self.correctWords(self.mat.lemmatizeWords(toPR))
        toComparativeL = self.correctWords(self.mat.lemmatizeWords(toComparative))
        toSuperlativeL = self.correctWords(self.mat.lemmatizeWords(toSuperlative))

        # Inflect nouns:
        singulars = self.correctWords(self.mat.inflectNouns(toSingularL, 'singular'))
        plurals = self.correctWords(self.mat.inflectNouns(toPluralL, 'plural'))

        # Inflect verbs:
        papepas = self.correctWords(
            self.mat.conjugateVerbs(toPAPEPAL, 'PAST_PERFECT_PARTICIPLE', 'FIRST_PERSON_SINGULAR'))
        pas = self.correctWords(self.mat.conjugateVerbs(toPAL, 'PAST', 'FIRST_PERSON_SINGULAR'))
        prpas = self.correctWords(self.mat.conjugateVerbs(toPRPAL, 'PRESENT_PARTICIPLE', 'FIRST_PERSON_SINGULAR'))
        papas = self.correctWords(self.mat.conjugateVerbs(toPAPAL, 'PAST_PARTICIPLE', 'FIRST_PERSON_SINGULAR'))
        pes = self.correctWords(self.mat.conjugateVerbs(toPEL, 'PERFECT', 'FIRST_PERSON_SINGULAR'))
        prs = self.correctWords(self.mat.conjugateVerbs(toPRL, 'PRESENT', 'THIRD_PERSON_SINGULAR'))

        # Inflect adjectives and adverbs:
        comparatives = self.correctWords(self.mat.inflectAdjectives(toComparativeL, 'comparative'))
        superlatives = self.correctWords(self.mat.inflectAdjectives(toSuperlativeL, 'superlative'))

        # Create maps:
        stemM = {}
        singularM = {}
        pluralM = {}
        papepaM = {}
        paM = {}
        prpaM = {}
        papaM = {}
        peM = {}
        prM = {}
        comparativeM = {}
        superlativeM = {}

        for i in range(0, len(toNothing)):
            stemM[toNothing[i]] = toNothingL[i]
        for i in range(0, len(targets)):
            stemM[targets[i]] = targetsL[i]
        for i in range(0, len(toSingular)):
            stemM[toSingular[i]] = toSingularL[i]
            singularM[toSingular[i]] = singulars[i]
        for i in range(0, len(toPlural)):
            stemM[toPlural[i]] = toPluralL[i]
            pluralM[toPlural[i]] = plurals[i]
        for i in range(0, len(toPAPEPA)):
            stemM[toPAPEPA[i]] = toPAPEPAL[i]
            papepaM[toPAPEPA[i]] = papepas[i]
        for i in range(0, len(toPA)):
            stemM[toPA[i]] = toPAL[i]
            paM[toPA[i]] = pas[i]
        for i in range(0, len(toPRPA)):
            stemM[toPRPA[i]] = toPRPAL[i]
            prpaM[toPRPA[i]] = prpas[i]
        for i in range(0, len(toPAPA)):
            stemM[toPAPA[i]] = toPAPAL[i]
            papaM[toPAPA[i]] = papas[i]
        for i in range(0, len(toPE)):
            stemM[toPE[i]] = toPEL[i]
            peM[toPE[i]] = pes[i]
        for i in range(0, len(toPR)):
            stemM[toPR[i]] = toPRL[i]
            prM[toPR[i]] = prs[i]
        for i in range(0, len(toComparative)):
            stemM[toComparative[i]] = toComparativeL[i]
            comparativeM[toComparative[i]] = comparatives[i]
        for i in range(0, len(toSuperlative)):
            stemM[toSuperlative[i]] = toSuperlativeL[i]
            superlativeM[toSuperlative[i]] = superlatives[i]

            # Create final substitutions:
        final_substitutions = {}
        for target in tqdm.tqdm(subs, total=len(subs), leave=True):
            # Get lemma of target:
            targetL = stemM[target]

            # Create instance in final_substitutions:
            final_substitutions[target] = set([])

            # Iterate through pos tags of target:
            for pos in subs[target]:
                # Create final cands:
                final_cands = set([])

                # Get cands for a target and tag combination:
                cands = list(subs[target][pos])

                # Add candidates to lists:
                if pos == 'NN':
                    for cand in cands:
                        if targetL != stemM[cand]:
                            final_cands.add(singularM[cand])
                            final_cands.add(cand)
                elif pos == 'NNS':
                    for cand in cands:
                        if targetL != stemM[cand]:
                            final_cands.add(pluralM[cand])
                            final_cands.add(cand)
                elif pos == 'VB':
                    for cand in cands:
                        if targetL != stemM[cand]:
                            final_cands.add(papepaM[cand])
                elif pos == 'VBD':
                    for cand in cands:
                        if targetL != stemM[cand]:
                            final_cands.add(paM[cand])
                            final_cands.add(papaM[cand])
                elif pos == 'VBG':
                    for cand in cands:
                        if targetL != stemM[cand]:
                            final_cands.add(prpaM[cand])
                elif pos == 'VBN':
                    for cand in cands:
                        if targetL != stemM[cand]:
                            final_cands.add(paM[cand])
                            final_cands.add(papaM[cand])
                elif pos == 'VBP':
                    for cand in cands:
                        if targetL != stemM[cand]:
                            final_cands.add(peM[cand])
                elif pos == 'VBZ':
                    for cand in cands:
                        if targetL != stemM[cand]:
                            final_cands.add(prM[cand])
                elif pos == 'JJR' or pos == 'RBR':
                    for cand in cands:
                        if targetL != stemM[cand]:
                            final_cands.add(comparativeM[cand])
                elif pos == 'JJS' or pos == 'RBS':
                    for cand in cands:
                        if targetL != stemM[cand]:
                            final_cands.add(superlativeM[cand])
                else:
                    for cand in cands:
                        if targetL != stemM[cand]:
                            final_cands.add(cand)

                            # Add final cands to final substitutions:
                final_substitutions[target].update(final_cands)
        return final_substitutions

    def getExpandedSet(self, subs):
        # Create lists for inflection:
        nouns = set([])
        verbs = set([])
        adjectives = set([])

        # Fill lists:
        for target in subs:
            for pos in subs[target]:
                # Get cands for a target and tag combination:
                cands = list(subs[target][pos])

                # Add candidates to lists:
                if pos == 'NN' or pos == 'NNS':
                    nouns.add(target)
                elif pos.startswith('V'):
                    verbs.add(target)
                elif pos.startswith('J') or pos.startswith('RB'):
                    adjectives.add(target)

                    # Transform sets in lists:
        nouns = list(nouns)
        verbs = list(verbs)
        adjectives = list(adjectives)

        # Lemmatize words:
        nounsL = self.correctWords(self.mat.lemmatizeWords(nouns))
        verbsL = self.correctWords(self.mat.lemmatizeWords(verbs))
        adjectivesL = self.correctWords(self.mat.lemmatizeWords(adjectives))

        # Create lemma maps:
        nounM = {}
        verbM = {}
        adjectiveM = {}
        for i in range(0, len(nouns)):
            nounM[nouns[i]] = nounsL[i]
        for i in range(0, len(verbs)):
            verbM[verbs[i]] = verbsL[i]
        for i in range(0, len(adjectives)):
            adjectiveM[adjectives[i]] = adjectivesL[i]

            # Inflect words:
        plurals = self.correctWords(self.mat.inflectNouns(nounsL, 'plural'))
        pas = self.correctWords(self.mat.conjugateVerbs(verbsL, 'PAST'))
        prpas = self.correctWords(self.mat.conjugateVerbs(verbsL, 'PRESENT_PARTICIPLE'))
        papas = self.correctWords(self.mat.conjugateVerbs(verbsL, 'PAST_PARTICIPLE'))
        prs = self.correctWords(self.mat.conjugateVerbs(verbsL, 'PRESENT'))
        comparatives = self.correctWords(self.mat.inflectAdjectives(adjectives, 'comparative'))
        superlatives = self.correctWords(self.mat.inflectAdjectives(adjectives, 'superlative'))

        # Create inflected maps:
        pluralM = {}
        paM = {}
        prpaM = {}
        papaM = {}
        prM = {}
        comparativeM = {}
        superlativeM = {}
        for i in range(0, len(nouns)):
            pluralM[nouns[i]] = plurals[i]
        for i in range(0, len(verbs)):
            paM[verbs[i]] = pas[i]
            prpaM[verbs[i]] = prpas[i]
            papaM[verbs[i]] = papas[i]
            prM[verbs[i]] = prs[i]
        for i in range(0, len(adjectives)):
            comparativeM[adjectives[i]] = comparatives[i]
            superlativeM[adjectives[i]] = superlatives[i]

            # Create extended substitutions:
        substitutions_extended = {}
        for target in subs:
            for pos in subs[target]:
                # Get cands for a target and tag combination:
                cands = list(subs[target][pos])

                # Add original to substitution dictionary:
                self.addToExtended(target, pos, cands, substitutions_extended)

                # Add candidates to lists:
                if pos == 'NN':
                    pluralT = pluralM[target]
                    self.addToExtended(pluralT, 'NNS', cands, substitutions_extended)
                elif pos == 'NNS':
                    singularT = nounM[target]
                    self.addToExtended(singularT, 'NN', cands, substitutions_extended)
                elif pos == 'VB':
                    paT = paM[target]
                    prpaT = prpaM[target]
                    papaT = papaM[target]
                    prT = prM[target]
                    self.addToExtended(paT, 'VBD', cands, substitutions_extended)
                    self.addToExtended(prpaT, 'VBG', cands, substitutions_extended)
                    self.addToExtended(papaT, 'VBN', cands, substitutions_extended)
                    self.addToExtended(prT, 'VBP', cands, substitutions_extended)
                    self.addToExtended(prT, 'VBZ', cands, substitutions_extended)
                elif pos == 'VBD':
                    lemmaT = verbM[target]
                    prpaT = prpaM[target]
                    papaT = papaM[target]
                    prT = prM[target]
                    self.addToExtended(lemmaT, 'VB', cands, substitutions_extended)
                    self.addToExtended(prpaT, 'VBG', cands, substitutions_extended)
                    self.addToExtended(papaT, 'VBN', cands, substitutions_extended)
                    self.addToExtended(prT, 'VBP', cands, substitutions_extended)
                    self.addToExtended(prT, 'VBZ', cands, substitutions_extended)
                elif pos == 'VBG':
                    lemmaT = verbM[target]
                    paT = paM[target]
                    papaT = papaM[target]
                    prT = prM[target]
                    self.addToExtended(lemmaT, 'VB', cands, substitutions_extended)
                    self.addToExtended(paT, 'VBD', cands, substitutions_extended)
                    self.addToExtended(papaT, 'VBN', cands, substitutions_extended)
                    self.addToExtended(prT, 'VBP', cands, substitutions_extended)
                    self.addToExtended(prT, 'VBZ', cands, substitutions_extended)
                elif pos == 'VBN':
                    lemmaT = verbM[target]
                    paT = paM[target]
                    prpaT = prpaM[target]
                    prT = prM[target]
                    self.addToExtended(lemmaT, 'VB', cands, substitutions_extended)
                    self.addToExtended(paT, 'VBD', cands, substitutions_extended)
                    self.addToExtended(prpaT, 'VBG', cands, substitutions_extended)
                    self.addToExtended(prT, 'VBP', cands, substitutions_extended)
                    self.addToExtended(prT, 'VBZ', cands, substitutions_extended)
                elif pos == 'VBP':
                    lemmaT = verbM[target]
                    paT = paM[target]
                    prpaT = prpaM[target]
                    papaT = prM[target]
                    self.addToExtended(target, 'VBZ', cands, substitutions_extended)
                    self.addToExtended(lemmaT, 'VB', cands, substitutions_extended)
                    self.addToExtended(paT, 'VBD', cands, substitutions_extended)
                    self.addToExtended(prpaT, 'VBG', cands, substitutions_extended)
                    self.addToExtended(papaT, 'VBN', cands, substitutions_extended)
                elif pos == 'VBZ':
                    lemmaT = verbM[target]
                    paT = paM[target]
                    prpaT = prpaM[target]
                    papaT = prM[target]
                    self.addToExtended(target, 'VBP', cands, substitutions_extended)
                    self.addToExtended(lemmaT, 'VB', cands, substitutions_extended)
                    self.addToExtended(paT, 'VBD', cands, substitutions_extended)
                    self.addToExtended(prpaT, 'VBG', cands, substitutions_extended)
                    self.addToExtended(papaT, 'VBN', cands, substitutions_extended)
                elif pos == 'JJ':
                    comparativeT = comparativeM[target]
                    superlativeT = superlativeM[target]
                    self.addToExtended(comparativeT, 'JJR', cands, substitutions_extended)
                    self.addToExtended(superlativeT, 'JJS', cands, substitutions_extended)
                elif pos == 'JJR':
                    lemmaT = adjectiveM[target]
                    superlativeT = superlativeM[target]
                    self.addToExtended(lemmaT, 'JJ', cands, substitutions_extended)
                    self.addToExtended(superlativeT, 'JJS', cands, substitutions_extended)
                elif pos == 'JJS':
                    lemmaT = adjectiveM[target]
                    comparativeT = comparativeM[target]
                    self.addToExtended(lemmaT, 'JJ', cands, substitutions_extended)
                    self.addToExtended(comparativeT, 'JJR', cands, substitutions_extended)
                elif pos == 'RB':
                    comparativeT = comparativeM[target]
                    superlativeT = superlativeM[target]
                    self.addToExtended(comparativeT, 'RBR', cands, substitutions_extended)
                    self.addToExtended(superlativeT, 'RBS', cands, substitutions_extended)
                elif pos == 'RBR':
                    lemmaT = adjectiveM[target]
                    superlativeT = superlativeM[target]
                    self.addToExtended(lemmaT, 'RB', cands, substitutions_extended)
                    self.addToExtended(superlativeT, 'RBS', cands, substitutions_extended)
                elif pos == 'RBS':
                    lemmaT = adjectiveM[target]
                    comparativeT = comparativeM[target]
                    self.addToExtended(lemmaT, 'RB', cands, substitutions_extended)
                    self.addToExtended(comparativeT, 'RBR', cands, substitutions_extended)
        return substitutions_extended

    def getInitialSet(self, victor_corpus):
        substitutions_initial = {}
        lexf = open(victor_corpus)
        sents = []
        targets = []
        heads = []
        for line in lexf:
            data = line.strip().split('\t')
            sent = data[0].strip()
            target = data[1].strip()
            head = int(data[2].strip())
            sents.append(sent)
            targets.append(target)
            heads.append(head)
        lexf.close()

        tagged_sents = [];

        for sent in sents:
            result = self.tagger.pos_sentence(sent)
            tagged_sents.append([nltk.tag.str2tuple(t) for t in result.split()])

        for i in tqdm.tqdm(range(0, len(sents)), total=len(sents), leave=True):
            target = targets[i]
            head = heads[i]
            for word, pos in tagged_sents[i]:
                if word == target:
                    target_pos = pos;
            target_wnpos = self.getWordnetPOS(target_pos)

            syns = wn.synsets(target)

            cands = set([])
            for syn in syns:
                for lem in syn.lemmas():
                    candidate = self.cleanLemma(lem.name())
                    if len(candidate.split(' ')) == 1:
                        cands.add(candidate)
            if len(cands) > 0:
                if target in substitutions_initial:
                    substitutions_initial[target][target_pos] = cands
                else:
                    substitutions_initial[target] = {target_pos: cands}
        return substitutions_initial

    def addToExtended(self, target, tag, cands, subs):
        if target not in subs:
            subs[target] = {tag: cands}
        else:
            if tag not in subs[target]:
                subs[target][tag] = cands
            else:
                subs[target][tag].extend(cands)

    def correctWords(self, words):
        result = []
        for word in words:
            result.append(autocorrect.spell(word))
        return result

    def cleanLemma(self, lem):
        result = ''
        aux = lem.strip().split('_')
        for word in aux:
            result += word + ' '
        return result.strip()

    def getWordnetPOS(self, pos):
        if pos[0] == 'N' or pos[0] == 'V' or pos == 'RBR' or pos == 'RBS':
            return pos[0].lower()
        elif pos[0] == 'J':
            return 'a'
        else:
            return None
