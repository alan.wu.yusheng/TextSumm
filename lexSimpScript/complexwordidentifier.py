import nltk
from nltk.tokenize import RegexpTokenizer

from nltk_contrib.readability.textanalyzer import syllables_en
import nltk.data
import numpy as np
import pickle

FALL_BACK_SCORE=100

class ComplexWordIdentifer(object):

    def __init__(self, corpus, level=8, text=None, output=None):
        self.corpus = [w.lower() for w in corpus]
        self.fd = nltk.FreqDist(self.corpus)
        self.length = len(self.corpus)
        self.level = level
        self.data = text
        self.output = output;
        self.lm = pickle.load(open('models/word_grade','rb'))


    def __identify_trouble_words(self):
        wordsDiff = self.__classify_word_difficulty(self.__preprocess())

        words = wordsDiff[:,0]


        complex_words = words[wordsDiff[:,1].astype('float') > self.level]
        return complex_words

    def __classify_word_difficulty(self, words_list):

        difficulty =  np.empty([0,2])

        for x in words_list:
            value = [x, max(0, self.lm.predict(self.__find_features(x))[0][0])]
            difficulty = np.append(difficulty, [value], axis=0)

        return difficulty

    def __find_features(self, word):
        return np.array([self.__find_freq(word), len(word), syllables_en.count(word)]).reshape(1, -1)

    def __find_freq(self, word):
        freq_count = float(self.fd[word])
        freq = freq_count / self.length

        return freq

    def __preprocess(self):
        tokenizer = RegexpTokenizer(r'\w+')
        tokens = tokenizer.tokenize(self.data)
        # lower case all letter
        tokens = [w.lower() for w in tokens]

        return tokens

    def __convert_to_VICTOR(self, word_diff):
        tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
        sentence = tokenizer.tokenize(self.data)

        word_diff = set(word_diff)

        victor_array = []

        # print word_diff
        for s,sen in enumerate(sentence):
            senwords = [w.lower() for w in sen.split()]

            if set(senwords).intersection(word_diff):
                victor_entries = set(senwords).intersection(word_diff)

                for entry in victor_entries:
                    indices = [i for i, word in enumerate(senwords) if word == entry]
                    for i in indices:
                        victor_array.append((sen + '\t' + entry + '\t' + str(i) + '\n').encode('utf8'))

        with open(self.output, 'w') as outputfile:
            for v in victor_array:
                outputfile.write(v);

    def identify(self):
        trouble = self.__identify_trouble_words();
        self.__convert_to_VICTOR(trouble)

    def predict_aoa(self, word):
        candidate = word.split()
        score = []
        for x in candidate:
            score.append(max(0, self.lm.predict(self.__find_features(x))[0]))
        try:
            final_score = sum(score) / len(score)
        except:
            final_score = FALL_BACK_SCORE
        return final_score;

