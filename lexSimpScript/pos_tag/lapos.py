import pexpect

class Lapos:
    def __init__(self, lapos_root, model):
        self.child = pexpect.spawn("./" + lapos_root + "lapos -m " + "./" + lapos_root + model)
        self.child.readline()
        self.child.readline()

    def pos_sentence(self, string):
        self.child.sendline(string);
        self.child.readline()
        return self.child.readline()

    def end(self):
        self.child.terminate()
