import pywsd
from lexenstein.selectors import VoidSelector

class WSDSelector:
    def __init__(self, method):
        """
        Creates an instance of the WSDSelector class.

        @param method: Type of Word Sense Disambiguation algorithm to use.
        Options available:
        lesk - Original lesk algorithm.
        path - Path similarity algorithm.
        random - Random sense from WordNet.
        first - First sense from WordNet.
        """

        if method == 'lesk':
            self.WSDfunction = self.getLeskSense
        elif method == 'path':
            self.WSDfunction = self.getPathSense
        elif method == 'random':
            self.WSDfunction = self.getRandomSense
        elif method == 'first':
            self.WSDfunction = self.getFirstSense
        else:
            self.WSDfunction = self.getLeskSense

    def selectCandidates(self, substitutions, victor_corpus):
        """
        Selects which candidates can replace the target complex words in each instance of a VICTOR corpus.

        @param substitutions: Candidate substitutions to be filtered.
        It can be in two formats:
        A dictionary produced by a Substitution Generator linking complex words to a set of candidate substitutions.
        Example: substitutions['perched'] = {'sat', 'roosted'}
        A list of candidate substitutions selected for the "victor_corpus" dataset by a Substitution Selector.
        Example: [['sat', 'roosted'], ['easy', 'uncomplicated']]
        @param victor_corpus: Path to a corpus in the VICTOR format.
        For more information about the file's format, refer to the LEXenstein Manual.
        @return: Returns a vector of size N, containing a set of selected substitutions for each instance in the VICTOR corpus.
        """

        selected_substitutions = []

        substitution_candidates = []
        if isinstance(substitutions, list):
            substitution_candidates = substitutions
        elif isinstance(substitutions, dict):
            void = VoidSelector()
            substitution_candidates = void.selectCandidates(substitutions, victor_corpus)
        else:
            print('ERROR: Substitutions are neither a dictionary or a list!')
            return selected_substitutions

        c = -1
        lexf = open(victor_corpus)
        for line in lexf:
            c += 1
            data = line.strip().split('\t')
            sent = data[0].strip()
            target = data[1].strip()
            head = int(data[2].strip())

            target_sense = self.WSDfunction.__call__(sent, target)

            candidates = substitution_candidates[c]

            selected_candidates = set([])
            for candidate in candidates:
                candidate_sense = None
                try:
                    unic = unicode(candidate)
                    candidate_sense = self.WSDfunction.__call__(self.getCandidateSentence(sent, candidate, head),
                                                                candidate)
                except UnicodeDecodeError:
                    candidate_sense = None
                if target_sense or not candidate_sense:
                    if not candidate_sense or candidate_sense == target_sense:
                        selected_candidates.add(candidate)
            selected_substitutions.append(selected_candidates)
        lexf.close()
        return selected_substitutions

    def getLeskSense(self, sentence, target):
        try:
            result = pywsd.lesk.original_lesk(sentence, target)
            return result
        except IndexError:
            return None

    def getPathSense(self, sentence, target):
        try:
            result = pywsd.similarity.max_similarity(sentence, target, option="path", best=False)
            return result
        except IndexError:
            return None

    def getRandomSense(self, sentence, target):
        try:
            result = pywsd.baseline.random_sense(target)
            return result
        except IndexError:
            return None

    def getFirstSense(self, sentence, target):
        try:
            result = pywsd.baseline.first_sense(target)
            return result
        except IndexError:
            return None

    def getMaxLemmaSense(self, sentence, target):
        try:
            result = pywsd.baseline.max_lemma_count(target)
            return result
        except IndexError:
            return None

    def getCandidateSentence(self, sentence, candidate, head):
        tokens = sentence.strip().split(' ')
        result = ''
        for i in range(0, head):
            result += tokens[i] + ' '
        result += candidate + ' '
        for i in range(head + 1, len(tokens)):
            result += tokens[i] + ' '
        return result.strip()

    def toVictorFormat(self, victor_corpus, substitutions, output_path, addTargetAsCandidate=False):
        """
        Saves a set of selected substitutions in a file in VICTOR format.

        @param victor_corpus: Path to the corpus in the VICTOR format to which the substitutions were selected.
        @param substitutions: The vector of substitutions selected for the VICTOR corpus.
        @param output_path: The path in which to save the resulting VICTOR corpus.
        @param addTargetAsCandidate: If True, adds the target complex word of each instance as a candidate substitution.
        """
        o = open(output_path, 'w')
        f = open(victor_corpus)
        for subs in substitutions:
            data = f.readline().strip().split('\t')
            sentence = data[0].strip()
            target = data[1].strip()
            head = data[2].strip()

            newline = sentence + '\t' + target + '\t' + head + '\t'

            for sub in subs:
                try:
                    newline += '0:' + sub + '\t'
                except:
                    continue
            o.write(newline.strip() + '\n')
        f.close()
        o.close()
