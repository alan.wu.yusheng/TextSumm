import os
import sys

def main():
    for file in os.listdir("gradelevelwordlist"):
        process_file("gradelevelwordlist/" + file)


def break_up_tab(data):
    new_list = []
    tablist = []
    for i, val in enumerate(data):
        if '\t' in val:
            tablist.append(i)
        else:
            new_list.append(val)

    for x in tablist:
        for word in data[x].split('\t'):
            new_list.append(word)

    return new_list


def remove_other(data):
    for i, val in enumerate(data):
        temp = val.lower()
        if ' ' in val:
            temp = temp.split(' ')[1]
        if '*' in val:
            temp = temp.replace('*', '')

        data[i] = temp

    return data

def preprocess_data(data):
    data = filter(None, data)
    data = break_up_tab(data)
    data = remove_other(data)

    data = set(data)

    return data

def process_file(file):
    with open(file, 'r') as raw:
        data=raw.read().split('\n')

    data = preprocess_data(data);
    print len(data)
    with open(file, "w") as text_file:
        for x in data:
            text_file.write(x+'\n')


if __name__ == '__main__':
    sys.exit(main())
