import itertools
import numpy as np
import gensim
import nltk
from nltk_contrib.readability.textanalyzer import syllables_en
import nltk.data
from sklearn import svm
from complexwordidentifier import ComplexWordIdentifer
import pickle
from gensim.models import Word2Vec
import spacy.en
import re

FALL_BACK_SIMILARITY = 0.1

LABELS = {
    'ENT': 'ENT',
    'PERSON': 'ENT',
    'NORP': 'ENT',
    'FAC': 'ENT',
    'ORG': 'ENT',
    'GPE': 'ENT',
    'LOC': 'ENT',
    'LAW': 'ENT',
    'PRODUCT': 'ENT',
    'EVENT': 'ENT',
    'WORK_OF_ART': 'ENT',
    'LANGUAGE': 'ENT',
    'DATE': 'DATE',
    'TIME': 'TIME',
    'PERCENT': 'PERCENT',
    'MONEY': 'MONEY',
    'QUANTITY': 'QUANTITY',
    'ORDINAL': 'ORDINAL',
    'CARDINAL': 'CARDINAL'
}

def represent_word(word):
    if word.like_url:
        return '%%URL|X'
    text = re.sub(r'\s', '_', word.text)
    tag = LABELS.get(word.ent_type_, word.pos_)
    if not tag:
        tag = '?'
    return text + '|' + tag


def transform_doc(doc):
    count = 0.
    for ent in doc.ents:
        ent.merge(ent.root.tag_, ent.text, LABELS[ent.label_].decode('unicode-escape'))
        count += 1.
        # if count % 500000:
        #     print ("{:.2%} finished".format(count/len(doc.ents)))
    for np in doc.noun_chunks:
        while len(np) > 1 and np[0].dep_ not in ('advmod', 'amod', 'compound'):
            np = np[1:]
        np.merge(np.root.tag_, np.text, np.root.ent_type_)
    strings = []
    for sent in doc.sents:
        if sent.text.strip():
            strings.append(' '.join(represent_word(w) for w in sent if not w.is_space))
    if strings:
        return '\n'.join(strings) + '\n'
    else:
        return ''


def transform_pairwise(X, y):
    """Transforms data into pairs with balanced labels for ranking
    Transforms a n-class ranking problem into a two-class classification
    problem. Subclasses implementing particular strategies for choosing
    pairs should override this method.
    In this method, all pairs are choosen, except for those that have the
    same target value. The output is an array of balanced classes, i.e.
    there are the same number of -1 as +1
    Parameters
    ----------
    X : array, shape (n_samples, n_features)
        The data
    y : array, shape (n_samples,) or (n_samples, 2)
        Target labels. If it's a 2D array, the second column represents
        the grouping of samples, i.e., samples with different groups will
        not be considered.
    Returns
    -------
    X_trans : array, shape (k, n_feaures)
        Data as pairs
    y_trans : array, shape (k,)
        Output class labels, where classes have values {-1, +1}
    """
    X_new = []
    y_new = []
    y = np.asarray(y)
    if y.ndim == 1:
        y = np.c_[y, np.ones(y.shape[0])]
    comb = itertools.combinations(range(X.shape[0]), 2)
    for k, (i, j) in enumerate(comb):
        if y[i, 0] == y[j, 0] or y[i, 1] != y[j, 1]:
            # skip if same target or different group
            continue
        X_new.append(X[i] - X[j])
        y_new.append(np.sign(y[i, 0] - y[j, 0]))
        # output balanced classes
        if y_new[-1] != (-1) ** k:
            y_new[-1] = - y_new[-1]
            X_new[-1] = - X_new[-1]
    return np.asarray(X_new), np.asarray(y_new).ravel()


class RankSVM(svm.LinearSVC):
    """Performs pairwise ranking with an underlying LinearSVC model
    Input should be a n-class ranking problem, this object will convert it
    into a two-class classification problem, a setting known as
    `pairwise ranking`.
    See object :ref:`svm.LinearSVC` for a full description of parameters.
    """

    def fit(self, X, y):
        """
        Fit a pairwise ranking model.
        Parameters
        ----------
        X : array, shape (n_samples, n_features)
        y : array, shape (n_samples,) or (n_samples, 2)
        Returns
        -------
        self
        """
        X_trans, y_trans = transform_pairwise(X, y)
        super(RankSVM, self).fit(X_trans, y_trans)
        return self

    def decision_function(self, X):
        return np.dot(X, self.coef_.ravel())

    def predict(self, X):
        """
        Predict an ordering on X. For a list of n samples, this method
        returns a list from 0 to n-1 with the relative order of the rows of X.
        The item is given such that items ranked on top have are
        predicted a higher ordering (i.e. 0 means is the last item
        and n_samples would be the item ranked on top).
        Parameters
        ----------
        X : array, shape (n_samples, n_features)
        Returns
        -------
        ord : array, shape (n_samples,)
            Returns a list of integers representing the relative order of
            the rows in X.
        """
        if hasattr(self, 'coef_'):
            return np.argsort(np.dot(X, self.coef_.ravel()))
        else:
            raise ValueError("Must call fit() prior to predict()")

    def score(self, X, y):
        """
        Because we transformed into a pairwise problem, chance level is at 0.5
        """
        X_trans, y_trans = transform_pairwise(X, y)
        return np.mean(super(RankSVM, self).predict(X_trans) == y_trans)


class PairWiseRanker:

    def __init__(self, corpus, model = None):
        if model != None:
            self.ranker = pickle.load(open(model, "rb" ));

        self.w2vmodel = gensim.models.Word2Vec.load('models/word2vec/vector260k.vct')
        self.corpus = [w.lower() for w in corpus]
        self.fd = nltk.FreqDist(self.corpus)
        self.length = len(self.corpus)
        self.cwi = ComplexWordIdentifer(corpus)
        self.aoa_dict = self.__create_aoa_dict('models/AoA.csv')

        self.nlp = spacy.en.English()

    def train_model(self, victor_corpus, output_path):
        data = self.__read_from_victor_format(victor_corpus)
        X, y = self.__prepare_data(data);

        self.ranker = RankSVM().fit(X, y);

        with open(output_path, 'w') as output_file:
            pickle.dump(self.ranker, output_file);

    def getRankings(self, candidate_file):
        candidates = self.__read_from_victor_format(candidate_file)
        X,y = self.__prepare_data(candidates)

        entries = [[] for x in range(len(candidates))]
        for i, cand in enumerate(y):
            entries[int(cand[1])].append(X[i])

        ranking = []
        for i, entry in enumerate(entries):
            initial = candidates[i]["candidates"]
            try:
                result = self.ranker.predict(entry)
                final = []
                for x in result:
                    final.append(initial[int(x)].split(':')[1])

                ranking.append(final)
            except:
                ranking.append(initial)

        return ranking


    def __read_from_victor_format(self, victor_corpus):
        data = []
        lexf = open(victor_corpus)
        for line in lexf:
            split = line.strip().split('\t')

            sent = split[0].lower()
            doc = self.nlp(sent.decode('unicode-escape'))
            target = split[1].lower()
            sent_tagged = transform_doc(doc).split()
            candidates = split[3:]
            query, pos = None, None

            for word in sent_tagged:
                split = word.split('|')
                multi_word = split[0].split('_')

                if split[0] == target:
                    query = word
                    pos = split[1]


                if len(multi_word) > 1:
                    if target in multi_word:
                        query = word
                        pos = split[1]

            if not query or not pos:
                print sent_tagged
                print target

            entry = {
                "sent": sent,
                "sent_tagged": sent_tagged,
                "query": query,
                "pos": pos,
                "target": target,
                "candidates": candidates
            }
            data.append(entry)
        lexf.close()

        return data


    def __prepare_data(self, data):
        features = np.empty([0,6])
        target = np.empty([0,2])
        for i, entry in enumerate(data):
            for cand in entry["candidates"]:
                candidate = cand.split(":")
                rank = candidate[0]
                word = candidate[1]
                pos = entry["pos"]

                features = np.append(features, [map(float, self.__find_features(word, entry["query"], pos))], axis=0);
                target = np.append(target, [map(float, [rank, i])], axis=0)

        return features, target

    def __find_freq(self, word):
        freq_count = float(self.fd[word])
        freq = freq_count / self.length

        return freq

    def __find_features(self, word, target, pos):
       try:
           wd = word + "|" + pos;
           similarity = self.w2vmodel.similarity(wd, target)
       except:
           similarity = FALL_BACK_SIMILARITY
       freq = self.__find_freq(word)
       length = len(word)
       syll_count = syllables_en.count(word)

       try:
           aoa_score = self.aoa_dict[word]
       except:
           aoa_score = self.cwi.predict_aoa(word)

       same_target = int(word == target)
       return [similarity, freq, length, aoa_score, syll_count, same_target]

    def __create_aoa_dict(self, aoa_dict):
        aoa = {}
        line = open(aoa_dict, 'rb')
        for word in line:
            data = word.strip().split(',')
            try:
                aoa[data[0]] = float(data[10])
            except:
                pass
        line.close()

        return aoa;