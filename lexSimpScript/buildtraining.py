import sys
import numpy as np
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.cross_validation import train_test_split
import csv
import pickle

from nltk_contrib.readability.textanalyzer import syllables_en

from nltk.corpus import gutenberg
import nltk



def main():
    # build_training_set('AoA_words.csv', 'dataset.txt')
    features = np.empty([0,3])
    target = []
    with open('dataset.txt','rb') as dataset:
        data_reader = csv.reader(dataset, delimiter=",")

        for row in data_reader:
            if not row[1] == "NA":
                features = np.append(features, [map(float, row[2:])], axis=0);
                target.append(float(row[1]))

    features.astype(float);
    target = np.array(target).astype(float);

    X_train, X_test, y_train, y_test = train_test_split(features, target, random_state=42)

    lm = GradientBoostingRegressor(max_depth=7)

    lm.fit(X_train, y_train)

    pickle.dump(lm, open('models/word_grade','w'));

    pred_train = lm.predict(X_train)
    pred_test = lm.predict(X_test)

    err_vector_train = y_train - pred_train;
    y_bar_train = y_train - np.mean(y_train)

    print "RMSE = ", np.sqrt(np.mean(err_vector_train ** 2))
    print "MAE = ", np.mean(np.absolute(err_vector_train))
    print "RAE = ", np.sum(np.absolute(err_vector_train))/np.sum(np.absolute(y_bar_train))
    print "RSE = ", np.sum(err_vector_train ** 2) / np.sum(y_bar_train ** 2)
    train_SST = np.sum(y_bar_train ** 2)
    train_SSR = np.sum((pred_train - np.mean(pred_train)) ** 2)
    print "COD = ", train_SSR/train_SST


    err_vector_test = y_test - pred_test;
    y_bar_test = y_test - np.mean(y_test)

    print "RMSE =", np.sqrt(np.mean((y_test - pred_test) ** 2))
    print "MAE = ", np.mean(np.absolute(err_vector_test))
    print "RAE = ", np.sum(np.absolute(err_vector_test))/np.sum(np.absolute(y_bar_test))
    print "RSE = ", np.sum(err_vector_test ** 2) / np.sum(y_bar_test ** 2)
    train_SST = np.sum(y_bar_test ** 2)
    train_SSR = np.sum((pred_test - np.mean(pred_test)) ** 2)
    print "COD = ", train_SSR/train_SST


def build_training_set(aoafile, outputfile):
    dataset = [];
    with open(aoafile, 'r') as file:
        data = file.read().split('\n')

    for x in data:
        word = x.split(',');
        if len(word) > 10:
            data = word_char(word)
            if data:
                dataset.append(data);

    with open(outputfile, 'w') as output:
        for data in dataset:
            data_string = ','.join(str(x) for x in data);
            data_string += '\n';
            output.write(data_string);


def get_frequency_data(file):
    freq = {}
    with open(file, 'r') as word_freq:
        freq_data = word_freq.read().split('\n')

    for y in freq_data:
        word = y.split('\t');

        try:
            if not freq.has_key(word[0].lower()):
                freq[word[0].lower()] = int(word[1])
            else:
                freq[word[0].lower()] += int(word[1])
        except:
            continue

    return freq

def get_lexical_characteristic(file):
    with open(file, 'r') as lex_prop:
        elp = lex_prop.read().split('\r\n');
    elp_data = {}
    for row in elp:
        sp =  row.split(',');
        if len(sp) > 1:
            if sp[5].strip('\"') == "NULL" or sp[6].strip('\"') == "NULL":
                sp[5] = "0"
                sp[6] = "0"


            if sp[11].strip('\"') == "NULL":
                sp[11] = "1";

            try:
                data = [
                    int(sp[1].strip('\"')),
                    int(sp[2].strip('\"')),
                    int(sp[3].strip('\"')),
                    int(sp[4].strip('\"')),
                    float(sp[5].strip('\"')),
                    float(sp[6].strip('\"')),
                    int(sp[7].strip('\"')),
                    float(sp[8].strip('\"')),
                    int(sp[11].strip('\"')),
                ]

                wd = sp[0].strip('\"').lower();

                elp_data[wd] = data;
            except:
                continue

    return elp_data;

def word_char(word):

    wd = word[0]

    freq = word_frequency(wd);
    if word[2] == "Err:512":
        word[2] = "0"

    orth_length = orthographic_length(wd);
    syll_count = syllables(wd);

    target = word[10];

    # if elp_data.has_key(wd):
    #     data = elp_data[wd];
    #
    #     ortho_n = data[1];
    #     phono_n = data[2];
    #     OLD = data[3];
    #     PLD = data[4];
    #     bg_sum = data[5];
    #     bg_mean = data[6];
    #     morph_n = data[7];
    #
    #     return [wd, target, freq, word[2], orth_length, ortho_n, phono_n, OLD, PLD, bg_sum, bg_mean, morph_n]
    # else:
    #     return None

    return [wd, target, freq, orth_length, syll_count]
    #


def word_frequency(word):
    freq_count = float(fd[word])
    freq = freq_count / length

    return freq

def orthographic_length(word):
    return len(word);

def syllables(word):
    return syllables_en.count(word)


corpus = [w.lower() for w in gutenberg.words()]

fd = nltk.FreqDist(corpus)

length = len(corpus)
#
elp_data = get_lexical_characteristic('elp.csv')
FREQUENCY = get_frequency_data('combined_wordfreq.txt')

if __name__ == '__main__':
    sys.exit(main())
