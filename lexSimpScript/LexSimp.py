from complexwordidentifier import ComplexWordIdentifer
import sys
from nltk.corpus import reuters
from nltk.corpus import wordnet
from lexenstein.morphadorner import MorphAdornerToolkit
from evaluators import eval
from generator.WNGenerator import *
from lexenstein.selectors import *
from selector.AluisioSelector import *
from selector.WordVectorSelector import *
from lexenstein.spelling import *
from lexenstein.rankers import *
from lexenstein.features import *
import pickle

tools_dir = '/home/candiii/dev/tools/'


def main():
    # with open('test.txt', 'r') as raw:
    #     data=raw.read().replace('\n', ' ').decode('utf8')
    #
    # CW = ComplexWordIdentifer(reuters.words(), 8, data, "fun.txt")
    # CW.identify()

    #==================================================

    # # Generate Substitutions
    # nc = NorvigCorrector('test.txt')
    # # #
    # m = MorphAdornerToolkit(tools_dir + 'MorphAdornerToolkit/')
    # #
    # # kg = MerriamGenerator(m, '5a7b123d-79c5-4f0e-ba7c-3fb899dae261',nc)
    # # kg = YamamotoGenerator(m, 'c14cf9be-c06c-4619-8f3b-b5e4122614e7',nc)
    # kg = WordnetGenerator(m, nc)
    # subs = kg.getSubstitutions('standard/DB.txt')
    #
    # toVictorFormat(subs, 'evaluation/DB_wn.txt')

    #==================================================

    # with open('evaluation/all_test.pickle', 'w') as file:n
    #     pickle.dump(subs, file)
    #
    # # This is for generator evaluation
    # subs = pickle.load( open("evaluation/all_test.pickle", "rb" ) )
    # ge = GeneratorEvaluator();
    #
    # potential, precision, recall, fmeasure = ge.evaluateGenerator('standard/DB.txt', subs)
    #

    # print potential, precision, recall, fmeasure



    # Select Substitutions

    # subs = pickle.load( open("evaluation/wnsubs-semeval-train.pickle", "rb" ) )
    # subs1 = pickle.load( open("evaluation/mgsubs-semeval-train.pickle", "rb" ) )
    # subs2 = pickle.load( open("evaluation/ygsubs-semeval-train","rb"))
    #
    # merged = set(subs.keys() + subs1.keys() + subs2.keys())
    #
    # final = {}
    #
    # for x in merged:
    #     first = set()
    #     second = set()
    #     third = set()
    #     if x in subs.keys():
    #         first = subs[x]
    #     if x in subs1.keys():
    #         second = subs1[x]
    #     if x in subs2.keys():
    #         third = subs2[x]
    #
    #     final[x] = first.union(second).union(third);
    #
    #
    # with open('evaluation/all-semeval-train.pickle', 'w') as file:
    #     pickle.dump(final, file)


    # subs = pickle.load( open("evaluation/wnsubs-semeval-train.pickle", "rb" ) )
    # #
    # # wvselect = WSDSelector('lesk')
    # #
    # wvselect = AluisioSelector('models/cond_prob.bin')
    # # wvselect = WordVectorSelector('models/word_vector_model.bin')
    # selected = wvselect.selectCandidates(subs, 'standard/semeval_train.txt')
    #
    # # se = SelectorEvaluator()
    # # potential, precision, recall, fmeasure = se.evaluateSelector('standard/semeval_train.txt', selected)
    # # print potential, precision, recall, fmeasure
    #
    # wvselect.toVictorFormat('standard/semeval_train.txt', selected, 'evaluation/semeval_train.txt')
    #
    # fe = FeatureEstimator()
    # fe.addLengthFeature('Complexity')
    # fe.addSenseCountFeature('Simplicity')
    # fe.addSyllableFeature(m, 'Complexity')
    #
    # # br = BoundaryRanker(fe)
    # # br = MetricRanker(fe)
    # br = BottRanker('models/lms.bin')
    #
    #
    # # br.trainRanker('training/semeval_train.txt',1,'log', 'l1', 0.1, 0.1, 0.001)
    # rankings = br.getRankings('evaluation/semeval_train.txt')
    #
    # # re = RankerEvaluator()
    # # t1,t2,t3,r1,r2,r3 = re.evaluateRanker('standard/semeval_train.txt', rankings)
    # #
    # # print t1,t2,t3,r1,r2,r3
    #

    rankings = toRankingFormat('evaluation/DeBelder_wn.txt')
    pe = eval.PipelineEvaluator()
    precision, accuracy, changed, trank1, trank2, trank3, recall1, recall2, recall3 = pe.evaluateAll('standard/DeBelder.txt', rankings)

    print "trank-at-i:", trank1, trank2, trank3
    print "recall-at-i:", recall1, recall2, recall3
    print "MAP, Recall, Changed:", precision, accuracy, changed

    # toVictorFormat('subs.txt', rankings, 'finalsubs.txt')


def toRankingFormat(victor_corpus):
    f = open(victor_corpus)
    result = []
    for line in f:
        # Get all substitutions in ranking instance:
        data = line.strip().split('\t')
        substitutions = data[3:len(data)]

        rankings = []

        for j in range(0, len(substitutions)):
            substitution = substitutions[j].split(':')[1].strip()
            rankings.append(substitution)

        # Add them to result:
        result.append(rankings)
    f.close()

    return result;


def toVictorFormat(subs, output_path):
    o = open(output_path, 'w')
    # f = open(victor_corpus)
    for entry in subs:
        sentence = entry["sent"]
        target = entry["target"]
        head = str(entry["head"])
        candidates = sorted(list(entry["candidates"]), key=lambda x: x[1], reverse=True)
        #     data = f.readline().strip().split('\t')
        #     sentence = data[0].strip()
        #     target = data[1].strip()
        #     head = data[2].strip()
        #
        newline = sentence + '\t' + target + '\t' + head + '\t'
        for i, sub in enumerate(candidates):
            newline += str(i+1) + ':' + sub[0] + '\t'
        o.write(newline.strip() + '\n')
    # f.close()
    o.close()


if __name__ == '__main__':
    sys.exit(main())
