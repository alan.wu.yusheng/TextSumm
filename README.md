# Thesaurus-Based Lexical Simplification
----
## Setup
---


Our implementation is in the LexSimpScript folder of our repository. In order to run the main code, you have to install

1. [LEXenstein package](https://github.com/ghpaetzold/LEXenstein) and all their dependencies
2. [NLTK](http://www.nltk.org/install.html)
3. [NLTK\_contrib](https://github.com/nltk/nltk_contrib)
4. [Spacy](https://spacy.io/)
5. [Gensim](https://radimrehurek.com/gensim/)

## Running Our Code
---
Our main script is named TestingScript.py. By uncommenting the various functions in the main() function, you can run various parts of the algorithm. The results of the algorithms will be saved in the results folder.
